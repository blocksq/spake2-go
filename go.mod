module gitlab.com/blocksq/spake2-go

go 1.12

require (
	github.com/stretchr/testify v1.4.0
	go.dedis.ch/kyber/v3 v3.0.4
	golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
	golang.org/x/lint v0.0.0-20190409202823-959b441ac422 // indirect
	gopkg.in/yaml.v2 v2.2.2
)
